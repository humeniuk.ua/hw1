
function functabs(number) {
    const divs = document.getElementsByClassName("services-container");
    for (let i = 0; i < divs.length; i++) {
      if (i !== number - 1) {
        divs[i].classList.add("hidden");
      } else {
        document.getElementById("divcontent" + number).classList.remove("hidden");
      }
    }
  }

  let arr = ['/css/image/newImage/1.jpg', '/css/image/newImage/2.jpg', '/css/image/newImage/3.jpg', '/css/image/newImage/4.jpg', '/css/image/newImage/5.jpg',
   '/css/image/newImage/6.jpg', '/css/image/newImage/7.jpg', '/css/image/newImage/8.jpg', '/css/image/newImage/5.jpg',
   '/css/image/newImage/6.jpg', '/css/image/newImage/7.jpg', '/css/image/newImage/8.jpg'
  ];
  let pictureContainer = document.getElementById("picture");

  function onetime(node, type, callback) {
    node.addEventListener(type, function(e) {
      e.target.removeEventListener(e.type, arguments.callee);
      return callback(e);
    });  
  }

 onetime(document.getElementById("loadPhoto"), "click", addImage);
  function addImage(){
    arr.forEach(i=>{
      let img = document.createElement("img");
      img.setAttribute("src", i);
      img.className = 'list-img-image';
      pictureContainer.appendChild(img);
  });
  }

let slider = $(".people-slider");
let sliderText = $(".people-persona");
let left = $(".left");
let right = $(".right");
let photo = $(".people-slider .photo-persona");
let start = 0;

for (let i = 1; i < sliderText.length; i++){
    $(sliderText[i]).css("display", "none");
}
$(photo[0]).css("margin-bottom", "10px");

left.on("click", function() {
    for (let i = 0; i < sliderText.length; i++){
        $(sliderText[i]).css("display", "none");
        $(photo[i]).css("margin-bottom", "0px");
    }
    if(start === 0){
        start = 3;
    }
    else{
        start--; 
    }
    $(sliderText[start]).fadeIn("slow");
    $(photo[start]).css("margin-bottom", "10px");
});
right.on("click", function() {
    for (let i = 0; i < sliderText.length; i++){
        $(sliderText[i]).css("display", "none");
        $(photo[i]).css("margin-bottom", "0px");
    }
    if (start === 3){
        start = 0;
    }
    else {
        start++;
    }
    $(sliderText[start]).fadeIn("slow");
    $(photo[start]).css("margin-bottom", "10px");
});

for(let i = 0; i < slider.length; i++){
    $(slider[i]).on("click", function() {
        for (let j = 0; j < sliderText.length; j++){
            $(sliderText[j]).css("display", "none");
            $(photo[j]).css("margin-bottom", "0px");
        }
        $(sliderText[i]).fadeIn("slow");
        $(photo[i]).css("margin-bottom", "10px");
        start = i;
    })
}

